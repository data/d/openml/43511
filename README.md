# OpenML dataset: Sundown-and-Sunup-Data-(2010-2020)

https://www.openml.org/d/43511

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
I wanted to use this dataset to combine with Toronto Police Homicide reports to determine how time of day (and specifically illumination) have an impact on crime.
Content
The dataset currently has Sunup and Sundown times for every date from 2010-2020.  This includes daylight savings adjustments so you should be able to use this dataset in direct comparisons to other datetimes.
Acknowledgements
The web scraping was done from www.timeanddate.com

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43511) of an [OpenML dataset](https://www.openml.org/d/43511). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43511/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43511/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43511/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

